package com.b208.postApp.models;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity

@Table(name="users")
public class User {
    @Id
    @GeneratedValue
    private  Long id;

    @Column
    private String username;

    @Column
    private String password;

    //This will represent the one side of the relationship
    //Link this user to every post that have the same user_id to the primary key of the user.
    @OneToMany(mappedBy = "user")
    @JsonIgnore //to prevent infinite recursion with bidirectional relationships.
    private Set<Post> posts;

    public User(){}

    public User(String username, String password){
        this.username = username;
        this.password = password;

    }

    public String getUsername(){
        return username;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public String getPassword(){
        return password;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public Long getId(){
        return id;
    }
    public Set<Post> getPosts(){
        return posts;
    }
}
